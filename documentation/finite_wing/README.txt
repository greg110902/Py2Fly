FlyPy Aerodynamics Package - Finite Wing Solver

 - Specify the wingspan b and the spanwise chord length distribution c(y)
 - Specify alpha, the geometric angle of attack of the wing
 - Specify alpha_0, the zero-lift angle of attack of the airfoil you are using
 - Specify N, the number of terms to use in the gamma series represnetation
 - Run the code using "python finite_wing.py"
 - Plots and data are saved in their respective sub folders