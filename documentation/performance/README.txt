FlyPy Performance Package

General Performance
 - Edit the aircraft parameters in performance.py
 - Run the code using "python performance.py"
 - Plots and data are saved in their respective sub folders

 Static Stability
 - Edit the aircraft parameters in static_stability.py
 - Run the code using "python static_stability.py"
 - Prints the static margin, yaw stability derivative, and roll stability derivative