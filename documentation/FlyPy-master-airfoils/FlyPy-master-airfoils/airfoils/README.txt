FlyPy Airfoils Package

 - Edit the airfoil parameters in the desired airfoil generator
 - Run the code using e.g. "python naca_4_digit.py"
 - Airfoil geometry plot, camberline, and coordinates are saved in the current directory